# 又是等待放假的一天
 
# every
## every() 方法用于检测数组所有元素是否都符合指定条件（通过函数提供）。
## every() 方法使用指定函数检测数组中的所有元素：
+ 如果数组中检测到有一个元素不满足，则整个表达式返回 false ，且剩余的元素不会再进行检测。
+ 如果所有元素都满足条件，则返回 true
```
'use strict'
var arr=['RED','blue','Pink'];
console.log(arr.every(function(s){
    return s.length>0; //输出true,因为数组中的每个元素都满足s.length>0
}));

console.log(arr.every(function(s){
    return s.toLocaleUpperCase===s;   //输出false,因为数组中的每个元素不都是大写
}));
```

# find
## find() 方法返回通过测试（函数内判断）的数组的第一个元素的值。
## find() 方法为数组中的每个元素都调用一次函数执行：
+ 当数组中的元素在测试条件时返回 true 时, find() 返回符合条件的元素，之后的值不会再调用执行函数。
+ 如果没有符合条件的元素返回 undefined
```
var arr=[10,20,30,40];
console.log(arr.find(function(s){
    return s>18;     //输出20,20是数组中第一个大于10的数
}));

console.log(arr.find(function(s){
    return s<5;      //输出undefined,因为数组中没有小于5的数
}));
```

# findIndex
## findIndex() 方法返回传入一个测试条件（函数）符合条件的数组第一个元素位置。
## findIndex() 方法为数组中的每个元素都调用一次函数执行：
+ 当数组中的元素在测试条件时返回 true 时, findIndex() 返回符合条件的元素的索引位置，之后的值不会再调用执行函数。
+ 如果没有符合条件的元素返回 -1
```
var arr =[10,20,30,40];
console.log(arr.findIndex(function(s){
    return s>18;        //输出1,因为20在数组中的索引为1
}));
console.log(arr.findIndex(function(s){
    return s<5;         //输出-1,因为找不到小于5的数
}));
```

# foreach 
## forEach()方法用于调用数组的每个元素，并将元素传递给回调函数
```
var arr=[10,20,30,40];
arr.forEach(console.log);
```

# 闭包
## 闭包指的是：能够访问另一个函数作用域的变量的函数。清晰的讲：闭包就是一个函数，这个函数能够访问其他函数的作用域中的变量
```
function count() {
    var arr = [];
    for (var i=1; i<=3; i++) {
        arr.push(function () {
            console.log(i);     //输出i的值为4
            return i * i;
        });
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];

console.log(f1());              //输出16    
console.log(f2());              //输出16
console.log(f3());              //输出16
```
## ⚠按我们的理解i应该为1，2，3，最后f1()为1,f2()为4,f3()为9,事实上却不是,i却为4,f1(),f2(),f3()都为16,这就是闭包中的坑（引用的变量发生了变化）
## 因为每个闭包函数访问变量i是count执行环境下的变量i，随着循环的结束，i已经变成4了，所以执行每个闭包函数，结果打印为4,最后经过i*i就等于16

## 如何改进这个问题呢
+ 方法是再创建一个函数，用该函数的参数绑定循环变量当前的值，无论该循环变量后续如何更改，已绑定到函数参数的值不变
```
function count() {
    var arr = [];
    for (var i=1; i<=3; i++) {
        arr.push((function (n) {
            return function () {
                console.log(i);
                return n * n;
            }
        })(i));
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];

console.log(f1());
console.log(f2());
console.log(f3());
```
