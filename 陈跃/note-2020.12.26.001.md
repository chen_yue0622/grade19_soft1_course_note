# 操作DOM
## 由于HTML文档被浏览器解析后就是一棵DOM树，要改变HTML的结构，就需要通过JavaScript来操作DOM。
![](./img/dom.png)

### 直接上手
![](./img/60.png)

### 第一种方法
```
若要调用他们的节点的话
'use strict'

let content=document.getElementById('content');   //调用的是整个div的content
console.log(content);       

let cl=document.getElementsByClassName('banner');   //调用的是div中class命名为banner的
console.log(cl);

let div=document.getElementsByTagName('div');   //调用的所有的div
console.log(div);
```
![](./img/61.png)

### 第二种方法
```
let left=document.querySelector('#left');
console.log(left);

let con=document.querySelector('p>font'); //调用的永远是p标签里第一个font标签
console.log(con);

let a=document.querySelectorAll('font')[1];//调用p标签里的第二个font标签,有点数组索引
console.log(a);

let cs=document.querySelectorAll('div'); //调用所有的div标签
console.log(cs);
```
![](./img/62.png)

# 更新DOM
## 当我们拿到一个节点后,我们可以对他进行修改
```
let con=document.querySelector('#left');
console.log(con);
con.innerHTML='<h1>第一种修改的方法</h1>';
con.innerText='<h1>第二种修改的方法</h1>';
con.textContent='<h1>第三种修改的方法</h1>';
```

## ⚠但是innerText和textContent两者的区别在于读取属性时，innerText不返回隐藏元素的文本，而textContent返回所有文本。
![](./img/63.png)
### 当给“想家”加上style="display: none;"他就变成了隐藏元素,下面代码与图片可以清楚看出二者的区别
```
let con=document.querySelector('p');
console.log(con.innerText);
console.log(con.textContent);
```
![](./img/64.png)

# 插入DOM
### 插入前是![](./img/65.png)
```
let js=document.querySelector('#js');
let list=document.querySelector('#list');
list.appendChild(js);
```
### 插入后是![](./img/66.png)