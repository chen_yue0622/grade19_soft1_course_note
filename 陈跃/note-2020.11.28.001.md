# 不记得是写笔记的第几天了
## 第一部分
### 讲了部分上节课让我们做的练习

## 第二部分
### 简单带过了多维数组
### 练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！
```
var arr = ['小明','小红', '大军',];
arr.sort();
if(arr.length==1){
    console.log(`欢迎${arr[0]}同学`);
}else if(arr.length==2){
    console.log(`欢迎${arr[0]}同学和${arr[1]}同学`);
}else if(arr.length==3){
    console.log(`欢迎${arr[0]},${arr[1]}和${arr[2]}同学`)
}
```

## 第三部分 
### 对象:JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成。
1.定义对象
```
var person ={
    name:'小明',
    age:18,
    school:'闽西职业技术学院',
    class:'软件一班',
    height:180,
}
```
⚠ 如果属性名中含有-符号,就必须要用''括起来
访问的时候得用['xxx']来访问,不能用常规的.来访问

2.如何访问对象属性
```
console.log(person.name)
```

3.由于js的对象是动态类型,所以我们可以自由的添加或删除属性
```
person.weight=180  //新增小明的体重为180
delete person.class  //删除小明的班级
如果删除一个不存在的属性,则会输出undefined,而不会报错
```

4.判断对象是否拥有某一属性
```
'age' in person //拥有这一属性,输出true
'birth' in person //不拥有这一属性,输出false
```

## 第四部分
### 条件判断
    (1)if 语句 - 只有当指定条件为 true 时，使用该语句来执行代码
    (2)if...else 语句 - 当条件为 true 时执行代码，当条件为 false 时执行其他代码
    (3)if...else if....else 语句- 使用该语句来选择多个代码块之一来执行

### 练习
```
小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
低于18.5：过轻
18.5-25：正常
25-28：过重
28-32：肥胖
高于32：严重肥胖    
用if...else...判断并显示结果：
```

```
var height =parseFloat(prompt('请输入你的身高（单位：米）'));
var weight =parseFloat(prompt('请输入你的体重（单位：千克）'));
var bmi=weight/(height*height);
if(bmi<18.5){
        console.log('过轻')
}else if(bmi>=18.5 && bmi<25){
        console.log('正常')
}else if(bmi>=25 && bmi<28){
        console.log('过重')
}else if(bmi>=28 && bmi<32){
        console.log('肥胖')
}else if(bmi>=32){
        console.log('高于32：严重肥胖')
}
```