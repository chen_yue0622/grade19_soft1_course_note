# 又是✍笔记的一天
## 一、简单带过了CentOS
## 二、闭包
### 利用闭包创建一个简单的计数器
```
'use strict'
function create_counter(init) {
    var x = init || 0;
    return {
        inc: function () {
            x += 1;
            return x;
        }
    }
}
var a=create_counter(7);
console.log(a.inc());
```

### 利用闭包把多参数的函数变成单参数的函数
```
function make_pow(n){
    return function(x){
        return Math.pow(x,n)
    }
}
var pow1=make_pow(5);
var pow2=make_pow(8);
console.log(pow1(2));
console.log(pow2(2));
```
## 三、箭头函数
    1.定义一个简单的箭头函数
    x=>x+x;
    这个简单的箭头函数就相当于
    function(x){
        return x+x;
    }

    2.如果参数不是一个,就得用()括起来
    (1)两个参数
    (x,y)=>x*2+y

    (2)无参数
    ()=>111

    (3)可变参数
    (x, y, ...rest) => {
    var i, sum = x + y;
    for (i=0; i<rest.length; i++) {
        sum += rest[i];
    }
    return sum;
    }

    3.⚠如果返回的是一个对象
    abc:x=>{fn:x}   //这样是错误的
    abc:x=>({fn:x})

    4.在之前的练习中,函数会对this的指向有问题,使用箭头函数就会改善这个问题
    var obj = {
    birth: 1990,
    getAge: function () {
        var b = this.birth; // 1990
        var fn = () => {
            return ()=>{
              return new Date().getFullYear() - this.birth;
            }       
        } 
        return fn();
    }
    };
    let a=obj.getAge();
    console.log(a());