# 胡哥发🔥的一天

# 常量:用const来定义常量
```
const a='这是一个常量';
a='可以改变常量的内容吗';  
console.log(a);
```
## 🈲不能改变常量a的值,会直接报错


# 结构赋值
```
var arr=['如果','也许','大概'];
var x=arr[0];
var x=arr[1];
var x=arr[2];
console.log(x);   //输出大概
```
## 如果把var换成let
```
let arr=['如果','也许','大概'];
let x=arr[0];
let x=arr[1];
let z=arr[2];
console.log(x);  //会报错,会提示x已存在赋值
```

## 如果数组本身还有嵌套，也可以通过下面的形式进行解构赋值
## ⚠要注意嵌套层次和位置要保持一致
```
let arr=[1,2,[3,4]];
let [a,b,[c,d]]=arr;
console.log(a);  //输出1
console.log(b);  //输出2
console.log(c);  //输出3
console.log(d);  //输出4
```

## 在赋值时也可以忽略某些元素,用逗号代替
```
let arr=[1,2,[3,4]];
let [,,[,d]]=arr;
console.log(d);  //输出4
```

## 也可以对对象属性进行赋值
```
let person={
    name:'小明',
    age:18,
}
let {name,age}=person;
console.log(name);  //输出小明
console.log(age);   //输出18
```

```
let person={
    name:'小明',
    age:18,
    address:{
        country:'China',
        city:'龙岩',
    },
    hobby:[
        '唱','跳','Rap'
    ]
}
let{name,address:{city},hobby:[a,,c]}=person;
console.log(city);  //输出龙岩
console.log(a);     //输出唱
console.log(c);     //输出Rap
```